#!/bin/bash

a=Hello
b=World

echo $a $b

dir=/home-1/class-2s18@jhu.edu

ls $dir

c='Hello World'
echo $c

# double quotes subsitute variables
d="Big $c"
echo $d

# save contents of command into a variable
e=$(ls $dir | wc -l )
echo $e
